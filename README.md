
# Bootloader_F103

UART bootloader for STM32F103.

### Introduction

A bootloader for STM32F103 which receives HEX and bin files and flashes a new firmware into the flash memory. The bootloader can also receive encrypted firmware files, which have to be encrypted using the Desktop Application for the UART bootloader for STM32F103.

### Desktop Application

For flashing and encrypting a Desktop Application is used.
Link to the Desktop Application:
https://gitlab.com/aldinh95/bootloader_f103_desktop_app/

### Configuration of the bootloader

All necessary settings can be found in **own_config.h**.
1. **USE_ENCRYPTION**
	- Indicates whether the bootloader can receive encrypted data or not.
2. **USE_RSA_KEY_EXCHANGE**
	- Indicates whether the bootloader receives the password for the encrypted file using 					RSA encryption or not.
3. **UART_BUFFER_SIZE**
	- Specifies the size of the uart ringbuffer.
4. **PACKET_SIZE**
	- Specifies the size of packets, when receiving a bin file.
5. **MAX_DATA_LENGTH**
	- Specifies the maximum size of the data field for HEX files.
6. **MAX_FAILS**
	- Specifies the number of maximum fails, after which the sending process will be cancelled.

By configuring the bootloader (especially **USE_ENCRYPTION** and **USE_RSA_KEY_EXCHANGE**) only the needed functions will be compiled. So the size of the bootloader can be reduced, if some components of it are not needed.

### Making own user application

To make an own user application the memory location in the linker script (**STM32F103C8Tx_FLASH.ld** in project folder) has to be changed.

**Example:**
```
FLASH (rx) : ORIGIN = 0x8003000
```
Also the vector table offset in **system_stm32f1xx.c** has to be set according to the memory location (offset, not absolute address!):

**Example:**
```
#define VECT_TAB_OFFSET 0x00003000U
```
Theses two values define the start address of the user application.

The two values have to be chosen so that they are between **USER_APP_START_ADDRESS** (defined in **own_config.h**)  and **USER_APP_END_ADDRESS** (defined in **flasher.h**).

### Flashing user application

The user application will be sent through the Desktop Application to the STM32F103.