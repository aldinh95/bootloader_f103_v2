/*
 * flasher.h
 *
 *  Created on: 29.06.2020
 *      Author: Acer-NitroBE
 */

#ifndef FLASHER_H_
#define FLASHER_H_

#include <stdint.h>
#include "stm32f1xx_hal.h"
#include "own_config.h"

/* End address of the user application */
#define USER_APP_END_ADDRESS	(uint32_t)(FLASH_BANK1_END -4u)

/* Status report for the flash processes. */
typedef enum{
	FLASHER_OK					=0x00u,		//Flashing successful
	FLASHER_ERROR_SIZE			=0x01u,		//HEX file too big
	FLASHER_ERROR_WRITE			=0x02u,		//Writing error
	FLASHER_ERROR_READBACK		=0x03u,		//Writing successful, but error at reading
	FLASHER_ERROR				=0xFFu,		//Generic error
}flasher_status;

flasher_status flash_erase();
flasher_status flash_write(uint32_t address, uint8_t *data);
flasher_status flash_write_startAddress(uint32_t startaddress);
void flasher_jump_to_app();

#endif /* FLASHER_H_ */
