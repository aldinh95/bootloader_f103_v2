/*
 * UartRingbuffer.h
 *
 *  Created on: 30.06.2020
 *      Author: Acer-NitroBE
 */

#ifndef UARTRINGBUFFER_H_
#define UARTRINGBUFFER_H_

#include "stm32f1xx_hal.h"
#include <stdint.h>
#include "own_config.h"

extern UART_HandleTypeDef huart1;
#define uart &huart1

/* Struct with the elements of the ringbuffer */
typedef struct {
	unsigned char buffer[UART_BUFFER_SIZE];
	volatile unsigned int head;
	volatile unsigned int tail;
} ring_buffer;


void Ringbuf_init(void);
uint8_t Uart_read(void);
void Uart_wait_and_read(uint8_t *data);
void Uart_write(uint8_t c);
void Uart_sendstring(const char *s);
void Uart_printbase(uint32_t n, uint8_t base);
uint8_t IsDataAvailable(void);
uint8_t Uart_peek();
void USART1_IRQHandler(void);	//ISR for the UART ringbuffer

void clearBuffer();

#endif /* UARTRINGBUFFER_H_ */
