/*
 * own_rand.h
 *
 *  Created on: 09.09.2020
 *      Author: Acer-NitroBE
 */

#ifndef OWN_RAND_H_
#define OWN_RAND_H_

#include "crypto.h"

uint8_t* getRandom();

#endif /* OWN_RAND_H_ */
