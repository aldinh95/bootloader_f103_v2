/*
 * aes_cbc.h
 *
 *  Created on: 20.08.2020
 *      Author: Acer-NitroBE
 */

#ifndef AES_CBC_H_
#define AES_CBC_H_

#include "stdio.h"
#include "crypto.h"


int32_t STM32_AES_CBC_Encrypt(uint8_t* InputMessage,
		uint32_t InputMessageLength, uint8_t *AES192_Key,
		uint8_t *InitializationVector, uint32_t IvLength,
		uint8_t *OutputMessage, uint32_t *OutputMessageLength);

int32_t STM32_AES_CBC_Decrypt(uint8_t* InputMessage,
		uint32_t InputMessageLength, uint8_t *AES128_Key,
		uint8_t *InitializationVector, uint32_t IvLength,
		uint8_t *OutputMessage, uint32_t *OutputMessageLength);


#endif /* AES_CBC_H_ */
