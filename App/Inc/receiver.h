/*
 * receiver.h
 *
 *  Created on: 25.06.2020
 *      Author: Acer-NitroBE
 */

#ifndef RECEIVER_H_
#define RECEIVER_H_

#include <stdint.h>
#include "UartRingbuffer.h"
#include "own_config.h"

/* Record types defined by the INTEL HEX format */
typedef enum {
	REC_DATA	= 0x00u, // Data
	REC_EOF		= 0x01u, // End Of File
	REC_ESA		= 0x02u, // Extended Segment Address
	REC_SSA		= 0x03u, // Start Segment Address
	REC_ELA		= 0x04u, // Extended Linear Address
	REC_SLA		= 0x05u  // Start Linear Address
} record_type;

/* Enum which contains all the messages needed for flashing the bootloader */
typedef enum {

	ACKN				= 0x06u,	//Acknowledgement
	CONNECT				= 0x80u,	//Connecting
	HEX_FILE			= 0x81u,	//Sending HEX file
	BIN_FILE			= 0x82u,	//Sending bin file
	CRYPT_FILE			= 0x83u,	//Sending an encrypted file
	SYNC				= 0x84u,	//Synchronous sending of a file
	ASYNC				= 0x85u,	//Asynchronous sending of a file
	REQUEST_DATA		= 0x86u,	//Requesting data
	LINE_OK				= 0x87u,	//No error in this line
	PACKET_OK			= 0x88u,	//No error in this packet
	FINISHED			= 0x89u,	//Finished receiving and no errors
	ADDRESS_ERROR		= 0x8Au,	//Address related error
	RECORD_TYPE_ERROR	= 0x8Bu, 	//Record type related error
	CHECKSUM_ERROR		= 0x8Cu, 	//Checksum related error
	FLASH_ERROR			= 0x8Du,	//Flash related error
	START_CODE_ERROR	= 0x8Eu,	//Start code wrong
	TERMINATOR_ERROR	= 0x8Fu,	//Terminator wrong
	MAX_ERROR			= 0x90u,	//Max number of errors reached
	GENERIC_ERROR		= 0xFFu,	//Generic error
} uart_message;

void start_bootloader();

#endif /* RECEIVER_H_ */
