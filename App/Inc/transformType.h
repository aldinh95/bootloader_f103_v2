/*
 * transformType.h
 *
 *  Created on: 03.09.2020
 *      Author: Acer-NitroBE
 */

#ifndef TRANSFORMTYPE_H_
#define TRANSFORMTYPE_H_

#include "stdio.h"
#include "own_config.h"


/* Functions for HEX files */
uint8_t char_to_hexnumber(uint8_t data);
uint8_t charArray_to_uint8_t(uint8_t* data);
uint16_t charArray_to_uint16_t(uint8_t* data);
uint8_t* char_Array_to_hex_Array(uint8_t* data, uint8_t length);

/* Function for bin files */
uint32_t charArray_to_uint32_t(uint8_t* data, uint8_t size);

#endif /* TRANSFORMTYPE_H_ */
