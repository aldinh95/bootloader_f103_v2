/*
 * own_config.h
 *
 *  Created on: 10.09.2020
 *      Author: Acer-NitroBE
 */

#ifndef OWN_CONFIG_H_
#define OWN_CONFIG_H_

/* Settings for encryption */
#define USE_ENCRYPTION 1			// The bootloader can receive encrypted files
#define USE_RSA_KEY_EXCHANGE 1		// Use RSA for key exchange for AES

/* Size of the ringbuffer */
#define UART_BUFFER_SIZE 128

/* Packet size for receiving bin files */
#define PACKET_SIZE				64u

/* The maximum size of the data field for HEX files*/
#define MAX_DATA_LENGTH			((uint8_t)0x10)
/* The maximum number of fail, after which process will be aborted */
#define MAX_FAILS				((uint8_t)5u)


#if !defined(USE_ENCRYPTION)
#error "USE_ENCRYPTION not defined"
#endif

#if !defined(USE_RSA_KEY_EXCHANGE)
#error "USE_ENCRYPTION not defined"
#endif

#if !defined(UART_BUFFER_SIZE)
#error "UART_BUFFER_SIZE not defined"
#endif

#if !defined(MAX_DATA_LENGTH)
#error "MAX_DATA_LENGTH not defined"
#endif

#if !defined(MAX_FAILS)
#error "MAX_FAILS not defined"
#endif

#if !defined(PACKET_SIZE)
#error "PACKET_SIZE not defined"
#endif

#if (PACKET_SIZE%16 != 0)
#error "Packetsize not a multiple of 16"
#endif

/* The version of the bootloader */
#if (USE_ENCRYPTION == 1) &&  (USE_RSA_KEY_EXCHANGE == 1)
	#define VERSION 1u
#elif (USE_ENCRYPTION == 1) && (USE_RSA_KEY_EXCHANGE == 0)
	#define VERSION 2u
#elif (USE_ENCRYPTION == 0) && (USE_RSA_KEY_EXCHANGE == 0)
	#define VERSION 3u
#elif (USE_ENCRYPTION == 0) && (USE_RSA_KEY_EXCHANGE == 1)
	#error "No encryption selected, but RSA key exchange selected"
#endif

/* Start address of the user application */
#if (VERSION == 1)
	#define USER_APP_START_ADDRESS	(uint32_t)0x08005400u
#elif (VERSION == 2)
	#define USER_APP_START_ADDRESS	(uint32_t)0x08003400u
#elif (VERSION == 3)
	#define USER_APP_START_ADDRESS	(uint32_t)0x08002000u
#endif


#endif /* OWN_CONFIG_H_ */
