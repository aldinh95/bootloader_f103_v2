/*
 * ownMain.h
 *
 *  Created on: 29.06.2020
 *      Author: Acer-NitroBE
 */

#ifndef OWNMAIN_H_
#define OWNMAIN_H_

#include "main.h"
#include "flasher.h"
#include "receiver.h"

void ownMainFunction();

#endif /* OWNMAIN_H_ */
