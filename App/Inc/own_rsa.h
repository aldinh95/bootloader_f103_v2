/*
 * own_rsa.h
 *
 *  Created on: 08.09.2020
 *      Author: Acer-NitroBE
 */

#ifndef OWN_RSA_H_
#define OWN_RSA_H_

#include "stdio.h"
#include "crypto.h"

typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

int32_t RSA_Encrypt(RSApubKey_stt *P_pPubKey,
                    const uint8_t *P_pInputMessage,
                    int32_t P_InputSize,
                    uint8_t *P_pOutput);

#endif /* OWN_RSA_H_ */
