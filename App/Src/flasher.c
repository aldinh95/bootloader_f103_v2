/*
 * flasher.c
 *
 *  Created on: 29.06.2020
 *      Author: Acer-NitroBE
 */

#include "flasher.h"

/* Defing the UART which will be used */
UART_HandleTypeDef huart1;
#define uart &huart1

/* Function pointer for the jump */
typedef void (*fnc_ptr)(void);

/**
 * @brief   Erases the part of the flash memory for the user application
 * @param   void
 * @return  status: Report about the success of the erasing process
 */
flasher_status flash_erase() {

	HAL_FLASH_Unlock();

	flasher_status status = FLASHER_ERROR;
	FLASH_EraseInitTypeDef erase_init;
	uint32_t error = 0u;

	erase_init.TypeErase = FLASH_TYPEERASE_PAGES;
	erase_init.PageAddress = (USER_APP_START_ADDRESS );
	erase_init.Banks = FLASH_BANK_1;
	// Calculate the number of pages from "address" and the end of flash.
	erase_init.NbPages = (FLASH_BANK1_END - (USER_APP_START_ADDRESS ))
			/ FLASH_PAGE_SIZE + 1;

	// Do the actual erasing.
	if (HAL_OK == HAL_FLASHEx_Erase(&erase_init, &error)) {
		status = FLASHER_OK;
	}

	HAL_FLASH_Lock();

	return status;
}

/**
 * @brief   Writes the start address of the new firmware to the end of the flash memory
 * @param   startaddress: The start address of the new firmware
 * @return  status: Report about the success of the writing process
 */
flasher_status flash_write_startAddress(uint32_t startaddress) {

	flasher_status status = FLASHER_OK;

	HAL_FLASH_Unlock();

	/* The actual flashing. If there is an error, then report it. */
	if (HAL_OK
			!= HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, FLASH_BANK1_END - 3u,
					startaddress)) {
		return status |= FLASHER_ERROR_WRITE;
	}
	/* Read back the content of the memory. If it is wrong, then report an error. */
	if (startaddress != (*(volatile uint32_t*) (FLASH_BANK1_END - 3u))) {
		return status |= FLASHER_ERROR_READBACK;
	}

	HAL_FLASH_Lock();

	return status;
}

/**
 * @brief   Writes a word (32-bit) into the flash memory
 * @param   address: Address where the word has to be written
 * @param	*data: The word which has to be written into the flash
 * @return  status: Report about the success of the writing process
 */
flasher_status flash_write(uint32_t address, uint8_t *data) {

	flasher_status status = FLASHER_OK;

	uint32_t* data_32 = (uint32_t*) &data[0];

	HAL_FLASH_Unlock();

	//If we reached the end of the memory, then report an error and don't do anything else.
	if (USER_APP_END_ADDRESS <= address || address < USER_APP_START_ADDRESS) {
		return status |= FLASHER_ERROR_SIZE;
	} else {
		/* The actual flashing. If there is an error, then report it. */
		if (HAL_OK
				!= HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address,
						*data_32)) {
			return status |= FLASHER_ERROR_WRITE;
		}
		/* Read back the content of the memory. If it is wrong, then report an error. */
		if ((*data_32) != (*(volatile uint32_t*) address)) {
			return status |= FLASHER_ERROR_READBACK;
		}

	}

	HAL_FLASH_Lock();

	return status;
}

/**
 * @brief   Performs a jump to the user application
 * @param   void
 * @return  void
 */
void flasher_jump_to_app() {
	/* Load the start address form the flash memory*/
	uint32_t startaddress = (*(volatile uint32_t*) (FLASH_BANK1_END - 3u));

	/* Pointer to the application */
	fnc_ptr jump_to_app;
	jump_to_app =
			(fnc_ptr) (*(volatile uint32_t*) (startaddress + 4u));
	HAL_DeInit();
	/* Change the main stack pointer. */
	__set_MSP(*(volatile uint32_t*) startaddress);
	jump_to_app();
}
