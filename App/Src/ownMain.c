/*
 * ownMain.c
 *
 *  Created on: 07.07.2020
 *      Author: Acer-NitroBE
 */

#include "ownMain.h"

void ownMainFunction() {


	/* Start bootloader and check whether a new application has to be flashed or not */
	start_bootloader();

	/* Jump to new application */
	flasher_jump_to_app();

}
