/*
 * UartRingbuffer.h
 *
 *  Created on: 30.06.2020
 *      Author: Acer-NitroBE
 */

#include "UartRingbuffer.h"
#include <string.h>

/* Defing the UART which will be used */
UART_HandleTypeDef huart1;
#define uart &huart1

/* Global variables. */
ring_buffer rx_buffer = { { 0 }, 0, 0 };
ring_buffer tx_buffer = { { 0 }, 0, 0 };

ring_buffer *_rx_buffer;
ring_buffer *_tx_buffer;

/* Local function. */
void store_char(unsigned char c, ring_buffer *buffer);

/**
 * @brief   This function initializes the ringbuffer.
 * @param   void
 * @return  void
 */
void Ringbuf_init(void) {
	_rx_buffer = &rx_buffer;
	_tx_buffer = &tx_buffer;

	/* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
	__HAL_UART_ENABLE_IT(uart, UART_IT_ERR);

	/* Enable the UART Data Register not empty Interrupt */
	__HAL_UART_ENABLE_IT(uart, UART_IT_RXNE);
}

/**
 * @brief   Stores a char into the ringbuffer
 * @param   c: The char which has to be stored
 * @param   *buffer: Pointer to the ringbuffer
 * @return  void.
 */
void store_char(unsigned char c, ring_buffer *buffer) {

	/* Deactivate interrupt for UART, if interrupt is activated */
	uint8_t status = HAL_NVIC_GetActive(USART1_IRQn);
	if(	status == 1){
		HAL_NVIC_DisableIRQ(USART1_IRQn);
	}

	uint8_t i = (unsigned int) (buffer->head + 1) % UART_BUFFER_SIZE;

	// if we should be storing the received character into the location
	// just before the tail (meaning that the head would advance to the
	// current location of the tail), we're about to overflow the buffer
	// and so we don't write the character or advance the head.

	if (i != buffer->tail) {
		buffer->buffer[buffer->head] = c;
		buffer->head = i;
	}

	/* Activate interrupt for UART again, if interrupt was deactivated */
	if(status == 1){
		HAL_NVIC_EnableIRQ(USART1_IRQn);
	}

}

/**
 * @brief   Reads a uint8_t from a the ringbuffer
 * @param   void
 * @return  c: char c which has been read, 0xFF if empty
 */
uint8_t Uart_read(void) {

	uint8_t status = HAL_NVIC_GetActive(USART1_IRQn);
	if(	status == 1){
		HAL_NVIC_DisableIRQ(USART1_IRQn);
	}

	// if the head isn't ahead of the tail, we don't have any characters
	if (_rx_buffer->head == _rx_buffer->tail) {

		if(status == 1){
			HAL_NVIC_EnableIRQ(USART1_IRQn);
		}

		return 0xFF;
	} else {
		unsigned char c = _rx_buffer->buffer[_rx_buffer->tail];
		_rx_buffer->tail = (unsigned int) (_rx_buffer->tail + 1)
				% UART_BUFFER_SIZE;

		/* Activate interrupt for UART again, if interrupt was deactivated */
		if(status == 1){
			HAL_NVIC_EnableIRQ(USART1_IRQn);
		}

		return c;
	}
}

/**
 * @brief   Wait till there is a byte to read in the ringbuffer and read a uint8_t from a the ringbuffer
 * @param   data: The uint8_t where the byte will be saved
 * @return  void
 */
void Uart_wait_and_read(uint8_t *data){
	while (!IsDataAvailable())
		;
	*data = Uart_read();
}

/**
 * @brief   Writes a uint8_t into the ringbuffer
 * @param   c: The char which has to be written
 * @return	void
 */
void Uart_write(uint8_t c) {

	/* Deactivate interrupt for UART, if interrupt is activated */
	uint8_t status = HAL_NVIC_GetActive(USART1_IRQn);
	if(	status == 1){
		HAL_NVIC_DisableIRQ(USART1_IRQn);
	}

	if (c >= 0) {
		int i = (_tx_buffer->head + 1) % UART_BUFFER_SIZE;

		// If the output buffer is full, there's nothing for it other than to
		// wait for the interrupt handler to empty it a bit
		while (i == _tx_buffer->tail)
			;

		_tx_buffer->buffer[_tx_buffer->head] = (uint8_t) c;
		_tx_buffer->head = i;

		__HAL_UART_ENABLE_IT(uart, UART_IT_TXE); // Enable UART transmission interrupt
	}

	/* Activate interrupt for UART again, if interrupt was deactivated */
	if(status == 1){
		HAL_NVIC_EnableIRQ(USART1_IRQn);
	}

}

/**
 * @brief   Checks if data in the rx_buffer is available
 * @param   void
 * @return	status: Returns if data is available, 0 if not available
 */
uint8_t IsDataAvailable(void) {


	return (uint8_t) (UART_BUFFER_SIZE + _rx_buffer->head - _rx_buffer->tail)
			% UART_BUFFER_SIZE;

}

/**
 * @brief   Sends a string through UART
 * @param   *s: The string which has to be written
 * @return	void
 */
void Uart_sendstring(const char *s) {
	while (*s)
		Uart_write(*s++);
}

/**
 * @brief   Prints a number with any base
 * @param   n: The number which has to be printed
 * @param   base: The base of the number which has to be printed
 * @return	void
 */
void Uart_printbase(uint32_t n, uint8_t base) {
	char buf[8 * sizeof(long) + 1]; // Assumes 8-bit chars plus zero byte.
	char *s = &buf[sizeof(buf) - 1];

	*s = '\0';

	// prevent crash if called with base == 1
	if (base < 2)
		base = 10;

	do {
		unsigned long m = n;
		n /= base;
		char c = m - base * n;
		*--s = c < 10 ? c + '0' : c + 'A' - 10;
	} while (n);

	while (*s)
		Uart_write(*s++);
}


/**
 * @brief   Peek for the data in the rx buffer without incrementing the tail count
 * @param   void
 * @return	c: Returns the first character in the rx buffer
 */
uint8_t Uart_peek() {

	/* Deactivate interrupt for UART, if interrupt is activated */
	uint8_t status = HAL_NVIC_GetActive(USART1_IRQn);
	if(	status == 1){
		HAL_NVIC_DisableIRQ(USART1_IRQn);
	}

	if (_rx_buffer->head == _rx_buffer->tail) {

		/* Activate interrupt for UART again, if interrupt was deactivated */
		if(status == 1){
			HAL_NVIC_EnableIRQ(USART1_IRQn);
		}

		return 0xFF;
	} else {

		/* Activate interrupt for UART again, if interrupt was deactivated */
		if(status == 1){
			HAL_NVIC_EnableIRQ(USART1_IRQn);
		}

		return _rx_buffer->buffer[_rx_buffer->tail];
	}
}

/**
 * @brief   The ISR for the ringbuffer for sending and receiving data
 * @param   *huart: Pointer to the UART handler
 * @return	void
 */
void USART1_IRQHandler(void) {

	/* Deactivate interrupt for UART, if interrupt is activated */
	uint8_t status = HAL_NVIC_GetActive(USART1_IRQn);
	if(	status == 1){
		HAL_NVIC_DisableIRQ(USART1_IRQn);
	}


	uint32_t isrflags = READ_REG(huart1.Instance->SR);
	uint32_t cr1its = READ_REG(huart1.Instance->CR1);

	/* if DR is not empty and the Rx Int is enabled */
	if (((isrflags & USART_SR_RXNE) != RESET)
			&& ((cr1its & USART_CR1_RXNEIE) != RESET)) {
		/******************
		 *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
		 *          error) and IDLE (Idle line detected) flags are cleared by software
		 *          sequence: a read operation to USART_SR register followed by a read
		 *          operation to USART_DR register.
		 * @note   RXNE flag can be also cleared by a read to the USART_DR register.
		 * @note   TC flag can be also cleared by software sequence: a read operation to
		 *          USART_SR register followed by a write operation to USART_DR register.
		 * @note   TXE flag is cleared only by a write to the USART_DR register.

		 *********************/
		huart1.Instance->SR; /* Read status register */
		unsigned char c = huart1.Instance->DR; /* Read data register */
		store_char(c, _rx_buffer);  // store data in buffer

		/* Activate interrupt for UART again, if interrupt was deactivated */
		if(status == 1){
			HAL_NVIC_EnableIRQ(USART1_IRQn);
		}

		return;
	}

	/*If interrupt is caused due to Transmit Data Register Empty */
	if (((isrflags & USART_SR_TXE) != RESET)
			&& ((cr1its & USART_CR1_TXEIE) != RESET)) {
		if (tx_buffer.head == tx_buffer.tail) {
			// Buffer empty, so disable interrupts
			__HAL_UART_DISABLE_IT(&huart1, UART_IT_TXE);

		}

		else {
			// There is more data in the output buffer. Send the next byte
			unsigned char c = tx_buffer.buffer[tx_buffer.tail];
			tx_buffer.tail = (tx_buffer.tail + 1) % UART_BUFFER_SIZE;

			/******************
			 *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
			 *          error) and IDLE (Idle line detected) flags are cleared by software
			 *          sequence: a read operation to USART_SR register followed by a read
			 *          operation to USART_DR register.
			 * @note   RXNE flag can be also cleared by a read to the USART_DR register.
			 * @note   TC flag can be also cleared by software sequence: a read operation to
			 *          USART_SR register followed by a write operation to USART_DR register.
			 * @note   TXE flag is cleared only by a write to the USART_DR register.

			 *********************/

			huart1.Instance->SR;
			huart1.Instance->DR = c;

		}

		/* Activate interrupt for UART again, if interrupt was deactivated */
		if(status == 1){
			HAL_NVIC_EnableIRQ(USART1_IRQn);
		}
		return;
	}

	/* Activate interrupt for UART again, if interrupt was deactivated */
	if(status == 1){
		HAL_NVIC_EnableIRQ(USART1_IRQn);
	}
}
