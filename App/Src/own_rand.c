/*
 * own_rand.c
 *
 *  Created on: 09.09.2020
 *      Author: Acer-NitroBE
 */

#include "own_rand.h"
#include "UartRingbuffer.h"

uint8_t* getRandom() {
	uint8_t entropy_data[16] = { 0, };
	/* Nonce. Non repeating sequence, such as a timestamp */
	uint8_t nonce[8] = { 0, };
	/* Personalization String */
	uint8_t personalization_String[8] = { 0, };

	/* Array that will be filled with random bytes */
	static uint8_t RandomString[16] = { 0, };

	/* Fill the needed arrays with data received from the desktop application */

	for(int i = 0; i<16;i++){
		Uart_wait_and_read(&entropy_data[i]);
	}
	for(int i = 0; i<8;i++){
		Uart_wait_and_read(&nonce[i]);
	}
	for(int i = 0; i<8;i++){
		Uart_wait_and_read(&personalization_String[i]);
	}


	RNGstate_stt RNGstate;

	RNGinitInput_stt RNGinit_st;

	int32_t status = RNG_SUCCESS;

	/* Set the values of EntropyData, Nonce, Personalization String and their sizes inside the RNGinit_st structure */
	RNGinit_st.pmEntropyData = entropy_data;
	RNGinit_st.mEntropyDataSize = sizeof(entropy_data);
	RNGinit_st.pmNonce = nonce;
	RNGinit_st.mNonceSize = sizeof(nonce);
	RNGinit_st.pmPersData = personalization_String;
	RNGinit_st.mPersDataSize = sizeof(personalization_String);

	status = RNGinit(&RNGinit_st, &RNGstate);
	if (status == RNG_SUCCESS) {
		/* The Random engine has been initialized, the status is in RNGstate */

		/* Now fill the random string with random bytes */
		status = RNGgenBytes(&RNGstate, NULL, RandomString,
				16);

		if (status == RNG_SUCCESS) {
			/* Random Generated Succefully, free the state before returning */
			status = RNGfree(&RNGstate);

		}

	}
return RandomString;
}
