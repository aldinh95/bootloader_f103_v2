/*
 * transformType.c
 *
 *  Created on: 03.09.2020
 *      Author: Acer-NitroBE
 */

#include "transformType.h"

/**
 * @brief   Returns the hexadecimal value of a character
 * @param   data: Character which value has to be transformed into a hexadecimal number
 * @return  data: Hexadecimal value of the character
 */
uint8_t char_to_hexnumber(uint8_t data) {

//Numeric value of char in hexadecimal
	if (data <= '9') {	//For 0 - 9
		data -= '0';
	} else
		data -= 55;		//For A - F

	return data;
}

/**
 * @brief   Combines two hex digits to a uint8_t number
 * @param   data: Pointer to the two hex digits
 * @return  result: The combined uint8_t number
 */
uint8_t charArray_to_uint8_t(uint8_t* data) {
	uint8_t result = (char_to_hexnumber(data[0]) << 4u)
			+ (char_to_hexnumber(data[1]));
	return result;
}

/**
 * @brief   Combines four hex digits to a uint16_t number
 * @param   data: Pointer to the four hex digits
 * @return  result: The combined uint16_t number
 */
uint16_t charArray_to_uint16_t(uint8_t* data) {
	uint16_t result = (char_to_hexnumber(data[0]) << 12u)
			+ (char_to_hexnumber(data[1]) << 8u)
			+ (char_to_hexnumber(data[2]) << 4u) + (char_to_hexnumber(data[3]));
	return result;
}

/**
 * @brief   Transforms the data of an array from char into hexadecimal values
 * @param   data*: Pointer to the char array which has to be transformed
 * @param   length: Length of the data field
 * @return  p_data: Pointer to the array with the hexadecimal values
 */
uint8_t* char_Array_to_hex_Array(uint8_t* data, uint8_t length) {

	static uint8_t p_data[MAX_DATA_LENGTH];

	for (int i = 0; i < length; i++) {
		p_data[i] = charArray_to_uint8_t(&data[i * 2]);
	}

	return p_data;
}

/**
 * @brief   Combines up to eight hex digits to a uint32_t number
 * @param   data: Pointer to array which contains the hex digits
 * @param   size: The number of hex digits
 * @return  result: The combined uint32_t number
 */
uint32_t charArray_to_uint32_t(uint8_t* data, uint8_t size) {
	uint32_t result = 0;

	/* Each hex digit is shifted by a certain number of positions and added to result */
	for (int i = 0; i < size; i++) {
		result += (char_to_hexnumber(data[i]) << (size - 1 - i) * 4u);
	}

	return result;

}
