/*
 * receiver.c
 *
 *  Created on: 25.06.2020
 *      Author: Acer-NitroBE
 */

#include <receiver.h>
#include "stdbool.h"
#include <string.h>
#include "stdio.h"
#include "receiver.h"
#include "flasher.h"
#include "transformType.h"

#include "main.h"

/* Include class for AES encryption, if encryption is activated */
#if defined(VERSION) && (VERSION != 3)
#include "aes_cbc.h"
#endif

/* Include classes for RSA encryption and random number generator, if RSA key exchange is activated */
#if defined(VERSION) && (VERSION == 1)
#include "own_rsa.h"
#include "own_rand.h"
#endif

/* Global variables */
static uint16_t current_line = 00u;		//Current line number of the hex file
static bool hex_received_first_data = false;//Indicates if first hex data was received
static uint16_t extended_segment_address = 0x00u;//Multiplied by 16 and added to each subsequent data record address
static uint16_t extended_linear_address = 0x00u;//Upper 16 bits of the 32 bit absolute address
static uint32_t hex_calculated_address = 00u;//Address for the current line of the hex file
static uint32_t hex_next_address = 00u;				//Address for the next line

static uint32_t address_bin = 0x00u;				//Address for the bin file
static uint32_t size_bin = 00u;						//Size of the bin file
static uint16_t current_packet = 00u;	//Current packet number of the bin file

#if defined(VERSION) && (VERSION != 3)
static uint16_t blockNumber = 0;			// Blocknumber of the AES encrypted file
static uint8_t iv[16] = { 0 };				// IV used for AES encryption
static uint8_t iv_last_packet[16] = { 0 };	// The IV of the last bin packet of the encrypted bin file
static uint8_t currentEncryptedIndex = 16u;	// Index of the current character, within the current block of the encrypted file
static uint8_t decryptedBuffer[16] = { 0 };	// Buffer with decrypted data
static uint8_t aeskey[16];					// The AES key
#endif

#if defined(VERSION) && (VERSION == 1)
static uint8_t output[2048 / 8];	//Output buffer for RSA Encryption
#endif

/* Local functions */
static void receive_file();

/* Receiving a hex file */
static uart_message receive_hex_file(bool async, bool encrypted);
static uart_message receive_hex_line(bool async, bool encrypted);

static void getHexChar(uint8_t* data, bool async, bool encrypted);

static uint32_t calc_address(uint16_t el_address, uint16_t es_address,
		uint16_t p_address);
static uart_message checkChecksum(uint8_t pbyte_count_hex,
		uint16_t paddress_hex, uint8_t precord_type_hex, uint8_t* pdata_hex,
		uint8_t pchecksum_hex);

/* Receiving a bin file */
static uart_message receive_bin_file(bool async, bool encrypted);
static uart_message receive_bin_packet(bool encrypted);

static void getBinInfo(bool encrypted);
static uint32_t crc32b(uint8_t* data, uint8_t cycles);
static void sync_bin(uint8_t* string);

/* Local functions for encrypted files */
#if defined(VERSION) && (VERSION != 3)
static void setIV(uint8_t* data, uint16_t blocknumber);
static uint32_t decryptBlock(uint8_t* decryptedArray);
static void getDecryptedHexChar(uint8_t* data, bool async);
static void getKey(uint8_t* data);
#endif

/**
 * @brief	Starts the bootloader and checks whether a new application has to be flashed or not
 * @param   void
 * @return  void
 */
void start_bootloader() {

	/* Check if the CONNECT message was received */
	uint8_t start = 0;
	HAL_UART_Receive(uart, &start, 1, 100u);
	if (CONNECT == start) {

		/*Initialize the ringbuffer, inform the desktop application that CONNECT was received and start flashing process */
		Ringbuf_init();
		Uart_write(ACKN);
		Uart_write(VERSION);
		Uart_write(PACKET_SIZE);
		receive_file();
	}
	/* Needed, so that the ringbuffer can inform the desktop application, when the whole flashing process was successful */
	HAL_Delay(10);
}

/**
 * @brief   Starts the receiving process of a firmware file, determines the file type which has to be received
 * @param   void
 * @return  void
 */
void receive_file() {

	uint8_t type;
	uint8_t mode;
	uart_message status = GENERIC_ERROR;

	/* Repeat the whole process until the process is successful */
	while (status != FINISHED) {

		/* Check whether a HEX or a bin file has to be received */
		type = 0;
		while (type == 0) {
			Uart_wait_and_read(&type);
		}

		/* Receiving a HEX file */
		if (HEX_FILE == type) {

			/* Checks whether the sending process will be synchronous or asynchronous */
			mode = 0;
			while (mode == 0) {
				Uart_wait_and_read(&mode);
			}

			/* Sending an acknowledgement and erase the flash memory */
			Uart_write(ACKN);
			flash_erase();

			/* Reset the needed variables */
			current_line = 00u;
			hex_received_first_data = false;
			extended_segment_address = 0x00u;
			extended_linear_address = 0x00u;
			hex_calculated_address = 00u;
			hex_next_address = 00u;

			/* Receive the hex file, synchronous or asynchronous according to the received mode, not encrypted */
			status = receive_hex_file(ASYNC == mode, false);
		}

		/* Receiving a bin file */
		if (BIN_FILE == type) {

			/* Checks whether the sending process will be synchronous or asynchronous */
			mode = 0;
			while (mode == 0) {
				Uart_wait_and_read(&mode);
			}

			/* Sending an acknowledgement and erase the flash memory */
			Uart_write(ACKN);
			flash_erase();

			/* Reset the needed variables */
			address_bin = 0x00u;
			size_bin = 00u;
			current_packet = 00u;

			/* Receive the bin file, synchronous or asynchronous according to the received mode, not encrypted */
			status = receive_bin_file(ASYNC == mode, false);
		}
/* Just compiled if encryption is activated */
#if defined(VERSION) && (VERSION != 3)
		if (CRYPT_FILE == type) {

			/* Check whether an encrypted HEX or an encrypted bin file has to be received */
			type = 0;
			while (type == 0) {
				Uart_wait_and_read(&type);
			}

			/* A encrypted bin file has to be received */
			if (BIN_FILE == type) {

				/* Checks whether the sending process will be synchronous or asynchronous */
				mode = 0;
				while (mode == 0) {
					Uart_wait_and_read(&mode);
				}

				/* Sending an acknowledgement and erase the flash memory */
				Uart_write(ACKN);
				flash_erase();

				/* Reset the needed variables */
				address_bin = 0x00u;
				size_bin = 00u;
				current_packet = 00u;
				blockNumber = 00u;

				/* Receive the bin file, synchronous or asynchronous according to the received mode, encrypted */
				status = receive_bin_file(ASYNC == mode, true);

			}
			/* A encrypted hex file has to be received */
			if (HEX_FILE == type) {
				/* Checks whether the sending process will be synchronous or asynchronous */
				mode = 0;
				while (mode == 0) {
					Uart_wait_and_read(&mode);
				}

				/* Sending an acknowledgement and erase the flash memory */
				Uart_write(ACKN);
				flash_erase();

				/* Reset the needed variables */
				current_line = 00u;
				hex_received_first_data = false;
				extended_segment_address = 0x00u;
				extended_linear_address = 0x00u;
				hex_calculated_address = 00u;
				hex_next_address = 00u;
				currentEncryptedIndex = 16u;
				blockNumber = 00u;
				current_packet = 00u;

				/* Receive the hex file, synchronous or asynchronous according to the received mode, encrypted */
				status = receive_hex_file(ASYNC == mode, true);
			}

		}
#endif
	}
}

/**
 * @brief   Starts the receiving process for a HEX file
 * @param   async: Indicates synchronous or asynchronous receiving
 * @param   encrypted: Indicates whether the file is encrypted or not
 * @return  line_status: Report about the success of receiving the HEX file (FINISHED or MAX_ERROR)
 */
uart_message receive_hex_file(bool async, bool encrypted) {

	uart_message line_status = LINE_OK;
	uint8_t fails = 0;

	/* String for synchronization, after the whole process failed. Synchronization is necessary for asynchronous mode and
	 * encryption mode, so that a new attempt can be started*/
	uint8_t syncString[8];

	/* Request actively date once at the beginning for asynchronous or encrypted receiving */
	if (async || encrypted)
		Uart_write(REQUEST_DATA);

	/* Receiving an encrypted hex file. Just compiled if encryption is enabled */
	if (encrypted) {
#if defined(VERSION) && (VERSION != 3)

		/* Get the sync String, if async mode */
		if (async) {
			for (int i = 0; i < 8; i++) {
				Uart_wait_and_read(&syncString[i]);
			}
		}

		/* Get the AES key */
		getKey(aeskey);
		/* Get the IV for decryption*/
		setIV(NULL, blockNumber);
		/* Check after every line if an error occured or the process finished */
		while (LINE_OK == line_status) {
			/* Receiving a line of the HEX file and process the data, encrypted = true, the process may be synchronous or asynchronous */
			line_status = receive_hex_line(async, true);
		}
		/* Inform the desktop application about the status of the last received line */
		Uart_write(line_status);
		if (FINISHED != line_status && async) {
			if (!async)
				Uart_printbase(current_packet, 10);	//The line number is sent as a string
			else
				Uart_printbase(current_packet-1, 10);	//The line number is sent as a string
			Uart_write(0xA);	//Needed: represents the end of the sent string

			/* Wait for the synchronization message, which the desktop application sends, after receiving the error type and line number.
			 * This is needed to synchronize the process again. After that a new process can be started.*/
			sync_bin(syncString);

		}
#endif
	}
	/* Receiving a non encrypted hex file */
	else {
		/* Check if finished or maximum fails reached */
		while (fails < MAX_FAILS && FINISHED != line_status) {
			/* Synchronous receiving */
			if (!async) {
				/* Request actively a line of the HEX file from the desktop application */
				Uart_write(REQUEST_DATA);
				/* Receiving a line of the HEX file and process the data */
				line_status = receive_hex_line(async, false);
			}
			/* Asynchronous receiving */
			else {
				/* Receive lines till there occurs an error or end of the file is reached (without requesting data after every line) */
				while (LINE_OK == line_status) {
					/* The receiving process fails completely if an error in a line occurred */
					line_status = receive_hex_line(async, false);
				}
			}

			/* Check if there was an error */
			if (line_status != LINE_OK && FINISHED != line_status) {
				/* Increment the number of fails */
				fails++;

				/* Check if number of maximum fails is reached */
				if (fails == MAX_FAILS)
					line_status = MAX_ERROR;
				else if (!async)
					current_line--;

				/* Asynchronous receiving: An error occurred-> inform the Desktop application in which line the error occurred
				 * and synchronize the two devices if max error not reached yet */
				if (async) {
					/* Decrement current line number, so that this line can be received again */
					current_line--;

					/*Inform the desktop application about the type of error and in which line the error occurred */
					Uart_write(line_status);
					Uart_printbase(current_line, 10); //The line number is sent as a string
					Uart_write(0xA); //Needed: represents the end of the sent string

					/* If the number of maximum fails is not reached, a new attempt is made to receive the line */
					if (fails < MAX_FAILS) {
						line_status = LINE_OK;
						/* Wait for the ACKN-message, which the desktop application sends, after receiving the error type and line number.
						 * This is needed to synchronize the process again. The bootloader starts reading at the beginning of a new line */
						while (Uart_read() != ACKN)
							;
					}
				}
			}

			/* Inform the desktop application about the status of the last received line */
			if (!async)
				Uart_write(line_status);
			else {
				/* Inform the desktop application if the process was successful */
				if (FINISHED == line_status) {
					Uart_write(line_status);
				}
			}

		}
	}
	/* Returns the status
	 * FINISHED: Jump to new application
	 * MAX_ERROR: Start new attempt for receiving a new firmware file (a different mode can be selected on the desktop application */
	return line_status;
}

/**
 * @brief   Starts the receiving process of a line of the HEX file
 * @param   async: Indicates synchronous or asynchronous receiving
 * @param   encrypted: Indicates whether the file is encrypted or not
 * @return  status: Report about the success of receiving a line from the HEX file
 */
uart_message receive_hex_line(bool async, bool encrypted) {
	uint8_t start_char = 0x0u;
	uint8_t byte_count_char[2];
	uint8_t address_char[4];
	uint8_t record_type_char[2];
	uint8_t data_char[2 * MAX_DATA_LENGTH];
	uint8_t checksum_char[2];
	uint8_t terminator_char[2];

// Get the start code
	getHexChar(&start_char, async, encrypted);

// Increment the line nu,ber
	current_line++;

// Check if the start code is right
	if (start_char == ':') {

		//Get byte count
		for (int i = 0; i < 2; i++) {
			getHexChar(&byte_count_char[i], async, encrypted);
		}
		/* Get the hexadecimal value of the byte count */
		uint8_t byte_count_hex = charArray_to_uint8_t(byte_count_char);

		//Get address
		for (int i = 0; i < 4; i++) {
			getHexChar(&address_char[i], async, encrypted);
		}
		/* Get the hexadecimal value of the address */
		uint16_t address_hex = charArray_to_uint16_t(address_char);

		//Get record type
		for (int i = 0; i < 2; i++) {
			getHexChar(&record_type_char[i], async, encrypted);
		}
		/* Get the hexadecimal value of the two record type */
		uint8_t record_type_hex = charArray_to_uint8_t(record_type_char);

		//Get data
		for (int i = 0; i < byte_count_hex * 2; i++) {
			getHexChar(&data_char[i], async, encrypted);
		}
		/* Get the hexadecimal value of the data */
		uint8_t* data_hex = char_Array_to_hex_Array(&data_char[0],
				byte_count_hex);

		//Get checksum
		for (int i = 0; i < 2; i++) {
			getHexChar(&checksum_char[i], async, encrypted);
		}
		/* Get the hexadecimal value of the checksum */
		uint8_t checksum_hex = charArray_to_uint8_t(checksum_char);

		//Get line terminator
		for (int i = 0; i < 2; i++) {
			getHexChar(&terminator_char[i], async, encrypted);
		}

		//Check line terminators
		if (terminator_char[0] != 0xD) {
			return TERMINATOR_ERROR; //return error if line terminators are wrong
		}
		if (terminator_char[1] != 0xA) {
			return TERMINATOR_ERROR; //return error if line terminators are wrong
		}

		//Check checksum
		if (LINE_OK
				!= checkChecksum(byte_count_hex, address_hex, record_type_hex,
						data_hex, checksum_hex)) {
			return CHECKSUM_ERROR;	//return error if checksum is wrong
		}

		/* Handling the data according to the record type */
		switch (record_type_hex) {
		case REC_DATA:

			/* Calculate the 32-bit address with the help of the extended linear address and the extended segment address */
			hex_calculated_address = calc_address(extended_linear_address,
					extended_segment_address, address_hex);

			flasher_status f_status = FLASHER_ERROR;

			/* Check if the calculated address is bigger than the last received address */
			if (hex_received_first_data == true
					&& hex_calculated_address < hex_next_address) {
				return ADDRESS_ERROR;
			}

			/* Address for check for the next line */
			hex_next_address = hex_calculated_address + byte_count_hex;

			/* Flashing the data into the flash memory word by word */
			for (int i = 0; i < byte_count_hex / 4; i++) {

				f_status = flash_write(hex_calculated_address + i * 4,
						&data_hex[0 + i * 4]);

				/* Stop if there was an error and report the type of the error */
				switch (f_status) {
				case FLASHER_OK:
					/* If first time receiving data there is no address of the previous data */
					if (hex_received_first_data == false) {
						hex_received_first_data = true;

						/* The address, where the first data is written to, is the start address of the new application */
						flash_write_startAddress(hex_calculated_address);
					}
					break;
				case FLASHER_ERROR_SIZE:
					return FLASH_ERROR;
					break;
				case FLASHER_ERROR_READBACK:
					return FLASH_ERROR;
					;
					break;
				case FLASHER_ERROR_WRITE:
					return FLASH_ERROR;
					break;
				case FLASHER_ERROR:
					return FLASH_ERROR;
					break;
				}
			}
			return LINE_OK;
			break;

		case REC_EOF:
			/* End of the file reached -> finished*/
			return FINISHED;
			break;

		case REC_ESA:
			/* Update the extended segment address */
			if (byte_count_hex == 0x2 && address_hex == 0x0) {
				extended_segment_address = (data_hex[0] << 8u) + (data_hex[1]);
			} else {
				return GENERIC_ERROR;
			}
			return LINE_OK;
			break;

		case REC_SSA:
			//Can be ignored for this type of STM32
			return LINE_OK;
			break;

		case REC_ELA:
			/* Update the extended linear address */
			if (byte_count_hex == 0x2 && address_hex == 0x0) {
				extended_linear_address = (data_hex[0] << 8u) + (data_hex[1]);
			} else {
				return GENERIC_ERROR;
			}
			return LINE_OK;
			break;

		case REC_SLA:
			//Can be ignored for this type of STM32
			return LINE_OK;
			break;

		default:
			/* Record type bigger than 0x05 */
			return RECORD_TYPE_ERROR;
		}

	} else {
		/* First character was not ":" */
		return START_CODE_ERROR;
	}
}

/**
 * @brief   Get the next char of the hex file
 * @param   data*: Pointer to the uint8_t variable, where the char has to be saved
 * @param   async: Indicates synchronous or asynchronous receiving
 * @param   encrypted: Indicates whether the file is encrypted or not
 * @return  void
 */
void getHexChar(uint8_t* data, bool async, bool encrypted) {

	/* The file is encrypted */
	if (encrypted) {
#if defined(VERSION) && (VERSION != 3)
		/* Get the char from the decrypted buffer*/
		getDecryptedHexChar(data, async);
#endif
		/* The file is not encrypted */
	} else
		/* Get the char directly from the UART ringbuffer */
		Uart_wait_and_read(data);
}

/**
 * @brief   Get the next char from the decrypted buffer
 * @param   data*: Pointer to the uint8_t variable, where the char has to be saved
 * @param   async: Indicates synchronous or asynchronous receiving
 * @return  void
 */
#if defined(VERSION) && (VERSION != 3)
void getDecryptedHexChar(uint8_t* data, bool async) {

	/* Check if the old block is completely read and a new block has to be decrypted (each AES block is 16 bytes long) */
	if (currentEncryptedIndex == 16) {
		/* Check if the receiving process is asynchronous */
		if (!async) {
			/* Request new data */
			Uart_write(LINE_OK);
			Uart_write(REQUEST_DATA);
		}
		/* Decrypt a new block and reset currentEncryptedIndex */
		decryptBlock(decryptedBuffer);
		currentEncryptedIndex = 0;
		current_packet++;
	}

	/* Save the next char from the buffer into "data" and increment currentEncryptedIndex */
	*data = decryptedBuffer[currentEncryptedIndex];
	currentEncryptedIndex++;

}
#endif

/**
 * @brief   Calculating a 32-bit address
 * @param	el_address: Extended linear address
 * @param	es_address: Extended segment address
 * @param	p_address: Lower 16 bits of the address (received from HEX file)
 * @return  address: the 32-bit address
 */
uint32_t calc_address(uint16_t el_address, uint16_t es_address,
		uint16_t p_address) {
	uint32_t address = (el_address << 16u) + (es_address * 16) + (p_address);

	return address;
}

/**
 * @brief   Checks if the received checksum is correct
 * @param	pbyte_count_hex: Hexadecimal value of the byte count
 * @param	paddress_hex: Hexadecimal value of the address
 * @param	precord_type_hex: Hexadecimal value of the record type
 * @param	*pdata_hex: pointer to the array with the Hexadecimal values of the data
 * @param	pchecksum_hex: Hexadecimal value of the checksum
 * @return  status: Reports whether the checksum is right or wrong
 */
uart_message checkChecksum(uint8_t pbyte_count_hex, uint16_t paddress_hex,
		uint8_t precord_type_hex, uint8_t* pdata_hex, uint8_t pchecksum_hex) {
	uart_message status = LINE_OK;

	uint16_t calc_Checksum = 0;

	uint8_t address_high = paddress_hex >> 8u;
	uint8_t address_low = paddress_hex & 0xFF;

	/* The checksum is right if the LSB of the sum of all elements including the checksum is 0 */

	calc_Checksum += pbyte_count_hex;
	calc_Checksum += address_high;
	calc_Checksum += address_low;
	calc_Checksum += precord_type_hex;

	for (int i = 0; i < pbyte_count_hex; i++) {
		calc_Checksum += pdata_hex[i];
	}

	calc_Checksum += pchecksum_hex;

	/* Get the LSB by bitwise AND with 0xFF ('1111 1111') */
	uint8_t LSB = calc_Checksum & 0xFF;

	if (LSB != 0u) {
		status = CHECKSUM_ERROR;
	}

	return status;
}

/**
 * @brief   Starts the receiving process for a bin file
 * @param   async: Indicates synchronous or asynchronous receiving
 * @param   encrypted: Indicates whether the file is encrypted or not
 * @return  line_status: Report about the success of receiving the bin file (FINISHED or MAX_ERROR)
 */
uart_message receive_bin_file(bool async, bool encrypted) {

	/* Request actively date once at the beginning */
	Uart_write(REQUEST_DATA);

	/* String for synchronization after an error occurred */
	uint8_t syncString[8];

	/* Get synchronization string */
	if (async) {
		for (int i = 0; i < 8; i++) {
			Uart_wait_and_read(&syncString[i]);
		}
	}

/* Just compiled if encryption is activated */
#if defined(VERSION) && (VERSION != 3)
	if (encrypted) {
		/* Get the AES key */
		getKey(aeskey);
		/* Get the IV for decryption if an encrypted file has to be received*/
		setIV(NULL, blockNumber);
	}
#endif

	/* Get the start address and the file size */
	getBinInfo(encrypted);

	/* Start the receiving process  */
	uart_message packet_status = PACKET_OK;
	uint8_t fails = 0;

	/* Check if finished or maximum fails reached */
	while (fails < MAX_FAILS && FINISHED != packet_status) {
		/* Receiving the file synchronously */
		if (!async) {
			/* Request actively a packet of the bin file from the desktop application */
			Uart_write(REQUEST_DATA);
			/* Receiving a packet of the HEX file and process the data */
			packet_status = receive_bin_packet(encrypted);
		}
		/* Receiving the file asynchronously */
		else {
			/* Receive packets till there occurs an error or end of the file is reached (without requesting data after every line) */
			while (PACKET_OK == packet_status) {
				packet_status = receive_bin_packet(encrypted);
			}
		}

		/* Check if there was an error */
		if (PACKET_OK != packet_status && FINISHED != packet_status) {
			/* Increment the number of fails */
			fails++;

			/* Check if number of maximum fails is reached */
			if (fails == MAX_FAILS)
				packet_status = MAX_ERROR;
			else if (!async)
				current_packet--;
			/* Asynchronous receiving: An error occurred-> inform the Desktop application in which packet the error occurred
			 * and synchronize the two devices if max error not reached yet */
			if (async) {

				current_packet--;

#if defined(VERSION) && (VERSION != 3)
				if (encrypted) {
					// Restore the IV of the last packet, so that the last packet can be received again
					for (int i = 0; i < 16; i++) {
						iv[i] = iv_last_packet[i];
					}
				}
#endif

				/*Inform the desktop application about the type of error and in which line the error occurred */
				Uart_write(packet_status);
				Uart_printbase(current_packet, 10);	//The line number is sent as a string
				Uart_write(0xA);//Needed: represents the end of the sent string

				/* If the number of maximum fails is not reached, a new attempt is made to receive the line */
				if (fails < MAX_FAILS) {
					packet_status = PACKET_OK;
				}
				/* Wait for the synchronization message, which the desktop application sends, after receiving the error type and line number.
				 * This is needed to synchronize the process again. The bootloader starts reading at the beginning of a new packet
				 * This message is more complex than the synchronization message for a hex file, because the data bytes of a bin file can have
				 * all possible values between 0x00 and 0xFF. So a message has to be chosen that is very unlikely to appear in the bin file */
				sync_bin(syncString);

			}
		}
		/* Inform the desktop application about the status of the last received packet */
		if (!async)
			Uart_write(packet_status);
		else {

			if (FINISHED == packet_status) {
				Uart_write(FINISHED);
			}
		}
	}
	/* Returns the status
	 * FINISHED: Jump to new application
	 * MAX_ERROR: Start new attempt for receiving a new firmware file (a different mode can be selected on the desktop application */

	return packet_status;
}

/**
 * @brief   Starts the receiving process for a packet of the bin file
 * @param   encrypted: Indicates whether the packet is encrypted or not
 * @return  Report about the success of receiving a packet from the bin file
 */
uart_message receive_bin_packet(bool encrypted) {

	/* Array which contains the received data */
	uint8_t data[((uint8_t)PACKET_SIZE)];

#if defined(VERSION) && (VERSION != 3)
	/* Contains the received decrypted CRC block */
	uint8_t crc_block[16];
#endif

	/* The CRC32-checksum as an array */
	uint8_t crc_received[8];
	/* The number of digits ot the crc checksum */
	uint8_t array_size = 0;

	/* Size of the current packet */
	uint16_t current_packet_size = 0;

	/* Increment current packet number */
	current_packet++;

	/* Calculate the size of the current packet. The last packet may be smaller */
	current_packet_size = (size_bin >= ((uint8_t)PACKET_SIZE)) ? ((uint8_t)PACKET_SIZE) : size_bin;

	/* The packet is not encrypted */
	if (!encrypted) {
		//Get data from UART ringbuffer
		for (int i = 0; i < current_packet_size; i++) {
			Uart_wait_and_read(&data[i]);
		}

		//Get crc from UART ringbuffer

		for (int i = 0; i <= 8; i++) {
			while (!IsDataAvailable())
				;
			// The end of the crc checksum is marked with CR (0xD)
			if (Uart_peek() == 0xD) {
				Uart_read();
				break;
			}
			array_size++;
			crc_received[i] = Uart_read();
		}
	}
	/* The packet is encrypted */
	else {
#if defined(VERSION) && (VERSION != 3)
		// Save the IV of the last block
		for (int i = 0; i < 16; i++) {
			iv_last_packet[i] = iv[i];
		}

		// Get the encrypted data and decrypt it
		for (int i = 0; i < ((uint8_t)PACKET_SIZE)/16; i++) {
			decryptBlock(&data[16 * i]);
		}

		//Get the encrypted crc and decrypt it
		decryptBlock(crc_block);

		// The crc checksum is 32 bit long -> 4 bytes. The 4 bytes are represented by 8 hex characters -> 8 bytes
		// The crc_block is 16 byte long. The end of the crc checksum within the crc_block is marked by a '?'.
		for (int i = 0; i < 8; i++) {
			if (crc_block[i] == '?') {
				break;
			}
			array_size++;
			crc_received[i] = crc_block[i];
		}
#endif
	}

	/* Convert the received array into an uint32_t */
	uint32_t crc_received_32 = charArray_to_uint32_t(crc_received, array_size);

	/* Calculate the CRC32 checksum, using the the function as the desktop application */
	uint32_t crc_calculated = crc32b(data, current_packet_size);

	/* Compare the two checksums */
	if (crc_received_32 == crc_calculated) {
		/* Start the flashing process, if the two checksums match */
		for (int i = 0; i < current_packet_size / 4; i++) {
			flasher_status flash_status = flash_write(address_bin,
					&data[i * 4]);
			/* Increment the address by 4 after every flashed word (32 bit) */
			address_bin += 4u;
			/* Return an error, if there was an error while flashing */
			if (FLASHER_OK != flash_status)
				return FLASH_ERROR;
		}
		/* Reduce the size of the bin file by the packet site, after everything was successful */
		size_bin -= current_packet_size;
		/* If the size_bin equals 0 after reducing the value, the last packet was received -> return FINISHED */
		if (size_bin != 0)
			return PACKET_OK;
		else
			return FINISHED;
	} else {
		/* The two checksums don't match */
		return CHECKSUM_ERROR;
	}

}

/**
 * @brief   Receive and save the start address and size of the bin file
 * @param   void
 * @return  void
 */
void getBinInfo(bool encrypted) {

	/* The size of the file, as an array */
	uint8_t file_size[8];
	/* The start address of the firmware, as an array */
	uint8_t address[8];
	/* The size of the data in the current array */
	uint8_t array_size = 0;

#if defined(VERSION) && (VERSION != 3)
	/* The decrypted file size and start address in an array */
	uint8_t decryptedBinInfo[16] = { 0 };
#endif

	/* The file is not encrypted */
	if (!encrypted) {
		/* Receive the size of the bin file as an char array with hexadecimal digits form the UART ringbuffer*/
		for (int i = 0; i <= 8; i++) {
			while (!IsDataAvailable())
				;
			if (Uart_peek() == 0xD) {
				Uart_read();
				break;
			}

			file_size[i] = Uart_read();
			array_size++;
		}

		/* Convert the received array into an uint32_t */
		size_bin = charArray_to_uint32_t(file_size, array_size);

		array_size = 0;

		/* Receive the start address for the bin file as an char array with hexadecimal digits */
		for (int i = 0; i <= 8; i++) {
			while (!IsDataAvailable())
				;
			if (Uart_peek() == 0xD) {
				Uart_read();
				break;
			}
			array_size++;
			address[i] = Uart_read();
		}

		/* Convert the received array into an uint32_t */
		address_bin = charArray_to_uint32_t(address, array_size);
	}
	/* The file is encrypted */
	else {
#if defined(VERSION) && (VERSION != 3)
		/* Decrypt a block */
		uint32_t status = decryptBlock(decryptedBinInfo);

		if (status == AES_SUCCESS) {

			/* The first 8 bytes of the block are reserved for the file size, the second 8 bytes for the start address.
			 * The end of both values is marked by a '?' */

			/* Get the bin file size from the received block */
			for (int i = 0; i < 8; i++) {
				if (decryptedBinInfo[i] == '?')
					break;
				file_size[i] = decryptedBinInfo[i];
				array_size++;
			}
			/* Convert the received array into an uint32_t */
			size_bin = charArray_to_uint32_t(file_size, array_size);

			/* Reset the variable */
			array_size = 0;

			/* Get the address from the received block */
			for (int i = 0; i < 8; i++) {
				if (decryptedBinInfo[i + 8] == '?')
					break;
				address[i] = decryptedBinInfo[i + 8];
				array_size++;
			}

			/* Convert the received array into an uint32_t */
			address_bin = charArray_to_uint32_t(address, array_size);
		}
#endif
	}
	/* Write the received start address to the flash memory */
	if (address_bin >= USER_APP_START_ADDRESS)
		flash_write_startAddress(address_bin);
}

/**
 * @brief   Calculating the CRC32-checksum
 * @param   data: Pointer to the two hex digits
 * @param   length: The length of the data array
 * @return  result: The CRC32-checksum of the data array
 */
uint32_t crc32b(uint8_t* data, uint8_t length) {

	/* This function has to be the same function, as the desktop application uses for calculating the CRC32-checksum */

	int i, j;
	unsigned int byte, crc, mask;

	i = 0;
	crc = 0xFFFFFFFF;
	while (length) {
		length--;
		byte = data[i];            // Get next byte.
		crc = crc ^ byte;
		for (j = 7; j >= 0; j--) {    // Do eight times.
			mask = -(crc & 1);
			crc = (crc >> 1) ^ (0xEDB88320 & mask);
		}
		i = i + 1;
	}
	return ~crc;
}

/**
 * @brief   Function which is used to synchronize the bootloader and the desktop application after an error occurred at receiving
 * 			a bin file asynchronous
 * @param   String: Pointer which cointains the synchronization message
 * @return  void
 */
void sync_bin(uint8_t* string) {

	/* Indicates whether the the two devices are synchronized again or not */
	bool synced = false;

	/* The length of the synchronization message */
	uint8_t length = 8;

	/* Repeat the search for the synchronization message, till the devices are synchronized again */
	while (!synced) {

		/* Label where the bootloader jumps when the the synchronization process is interrupted by a wrong byte */
		BEGIN:

		/* Wait for the first byte of the synchronization message */
		while (Uart_read() != string[0])
			;
		/* Check if the following bytes are correct */
		for (int i = 1; i < length; i++) {
			while (!IsDataAvailable())
				;
			/* Jump to BEGIN if a wrong byte was read */
			if (Uart_read() != string[i])
				goto BEGIN;
		}
		/* The last bytes has to be the new line byte */
		while (!IsDataAvailable())
			;
		if (Uart_read() != 0xA)
			goto BEGIN;
		else
			/* At this point the two devices are synchronized again */
			synced = true;
	}
}
/**
 * @brief   Determine the IV and set the value of the array "iv"
 * @param	iv*: The array in which the IV will be saved
 * @param   data*: The last received block of the file
 * @param	blocknumber: The number of the current block
 * @return  void
 */
#if defined(VERSION) && (VERSION != 3)				// Just compiled if encryption is activated
void setIV(uint8_t* data, uint16_t blocknumber) {

	/* Not the first block */
	if (blocknumber != 0) {
		/* For AES-CBC the IV for the next block is the last block */
		for (int i = 0; i < 16; i++) {
			iv[i] = data[i];
		}
	}
	/* IV for the first block */
	else {
		/* Receive the IV from the Desktop application */
		for (int i = 0; i < 16; i++) {
			Uart_wait_and_read(&iv[i]);
		}
	}
}
#endif
/**
 * @brief   Receive a 16 byte block and decrypt it
 * @param   decryptedArray*: The array in which the decrypted data will be saved
 * @return  status: The result of the decryption
 */
#if defined(VERSION) && (VERSION != 3)				// Just compiled if encryption is activated
uint32_t decryptBlock(uint8_t* decryptedArray) {

	uint8_t encrypted[16];

	/* Receive the encrypted data */
	for (int i = 0; i < 16; i++) {
		Uart_wait_and_read(&encrypted[i]);
	}

	/* Decrypt the block */
	uint32_t decryptedBinInfoLength = 0;

	int32_t status = AES_SUCCESS;
	status = STM32_AES_CBC_Decrypt(encrypted, 16, aeskey, iv, 16,
			decryptedArray, &decryptedBinInfoLength);

	/* Increment the blocknumber */
	if (status == AES_SUCCESS)
		blockNumber++;

	/* Set the IV */
	setIV(encrypted, blockNumber);

	return status;

}
#endif

/**
 * @brief   Set the AES key for the firmware file
 * @param   data*: The array in which the key will be saved
 * @return  void
 */
#if defined(VERSION) && (VERSION == 1)
void getKey(uint8_t* data) {

	/* RSA key exchange is activated */

	int32_t status = RSA_ERR_GENERIC;
	RSApubKey_stt PubKey_st;

	/* Get the exponent for the public key of the Desktop Application */
	uint8_t expLen;
	uint8_t exponent[3];
	Uart_wait_and_read(&expLen);

	for (int i = 0; i < expLen; i++) {
		Uart_wait_and_read(&exponent[i]);
	}

	/* Get the modulus for the public key of the Desktop Application */
	uint8_t modLen = 0;
	uint8_t modulus[128];
	Uart_wait_and_read(&modLen);

	for (int i = 0; i < modLen; i++) {
		Uart_wait_and_read(&modulus[i]);
	}

	/* Create a random temporary AES key */
	uint8_t* password_temp_new;
	password_temp_new = getRandom();

	/* Generate the public key */
	PubKey_st.mExponentSize = expLen;
	PubKey_st.mModulusSize = modLen;
	PubKey_st.pmExponent = (uint8_t *) exponent;
	PubKey_st.pmModulus = (uint8_t *) modulus;

	/* RSA encrypt the temporary AES key */
	status = RSA_Encrypt(&PubKey_st, password_temp_new, 16, output);

	if (status == RSA_SUCCESS) {

		HAL_Delay(100);

		/* Send the encrypted temporary AES key to the Desktop Application */
		for (int i = 0; i < 128; i++) {
			Uart_write(output[i]);
		}

		/* The IV for the encrypted AES paswword */
		uint8_t aesIV[16];

		/* The encrypted AES password for the file */
		uint8_t aesPass[16];

		/* Get the IV from the Desktop Application */
		for (int i = 0; i < 16; i++) {
			Uart_wait_and_read(&aesIV[i]);
		}

		/* Get the encrypted password */
		for (int i = 0; i < 16; i++) {
			Uart_wait_and_read(&aesPass[i]);
		}

		/* Decrypt the password with the temporary password */
		uint32_t aeskey_length = 0;
		STM32_AES_CBC_Decrypt(aesPass, 16, password_temp_new, aesIV, 16, data,
				&aeskey_length);
	}
}
#elif defined(VERSION) && (VERSION != 3)	/* Encryption is activated, RSA key exchange is not activated */
void getKey(uint8_t* data) {

	/* The deafult AES password can be set here */
	aeskey[0] = '0';
	aeskey[1] = '1';
	aeskey[2] = '2';
	aeskey[3] = '3';
	aeskey[4] = '4';
	aeskey[5] = '5';
	aeskey[6] = '6';
	aeskey[7] = '7';
	aeskey[8] = '8';
	aeskey[9] = '9';
	aeskey[10] = 'A';
	aeskey[11] = 'B';
	aeskey[12] = 'C';
	aeskey[13] = 'D';
	aeskey[14] = 'E';
	aeskey[15] = 'F';

}
#endif
